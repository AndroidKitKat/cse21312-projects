/**********************************************
* File: Vector.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#ifndef VECTOR_H
#define VECTOR_H

#include <algorithm>
#include <iostream>
#include <stdexcept>
#include "dsexceptions.h"

template <typename Object>
class Vector
{
  public:
    /********************************************
    * Function Name  : Vector
    * Pre-conditions :  int initSize = 0 
    * Post-conditions: explicit
    *  
    ********************************************/
    explicit Vector( int initSize = 0 )
      : theSize{ initSize }, theCapacity{ initSize + SPARE_CAPACITY }
      { objects = new Object[ theCapacity ]; }
      
    /********************************************
    * Function Name  : Vector
    * Pre-conditions :  const Vector & rhs 
    * Post-conditions: none
    *  
    ********************************************/
    Vector( const Vector & rhs )
      : theSize{ rhs.theSize }, theCapacity{ rhs.theCapacity }, objects{ nullptr }
    { 
        objects = new Object[ theCapacity ];
        for( int k = 0; k < theSize; ++k )
            objects[ k ] = rhs.objects[ k ];
    }
    
    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  const Vector & rhs 
    * Post-conditions: Vector &
    *  
    ********************************************/
    Vector & operator= ( const Vector & rhs )
    {
        Vector copy = rhs;
        std::swap( *this, copy );
        return *this;
    }
    
    /********************************************
    * Function Name  : ~Vector
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    ~Vector( )
      { delete [ ] objects; }

    /********************************************
    * Function Name  : Vector
    * Pre-conditions :  Vector && rhs 
    * Post-conditions: none
    *  
    ********************************************/
    Vector( Vector && rhs )
      : theSize{ rhs.theSize }, theCapacity{ rhs.theCapacity }, objects{ rhs.objects }
    {
        rhs.objects = nullptr;
        rhs.theSize = 0;
        rhs.theCapacity = 0;
    }
   
    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  Vector && rhs 
    * Post-conditions: Vector &
    *  
    ********************************************/
    Vector & operator= ( Vector && rhs )
    {    
        std::swap( theSize, rhs.theSize );
        std::swap( theCapacity, rhs.theCapacity );
        std::swap( objects, rhs.objects );
        
        return *this;
    }
    
    /********************************************
    * Function Name  : empty
    * Pre-conditions :  
    * Post-conditions: bool
    *  
    ********************************************/
    bool empty( ) const
      { return size( ) == 0; }

    /********************************************
    * Function Name  : size
    * Pre-conditions :  
    * Post-conditions: int
    *  
    ********************************************/
    int size( ) const
      { return theSize; }

    /********************************************
    * Function Name  : capacity
    * Pre-conditions :  
    * Post-conditions: int
    *  
    ********************************************/
    int capacity( ) const
      { return theCapacity; }

    /********************************************
    * Function Name  : operator[]
    * Pre-conditions :  int index 
    * Post-conditions: Object &
    *  
    ********************************************/
    Object & operator[]( int index )
    {
                                                     #ifndef NO_CHECK
        if( index < 0 || index >= size( ) )
            throw ArrayIndexOutOfBoundsException{ };
                                                     #endif
        return objects[ index ];
    }

    /********************************************
    * Function Name  : operator[]
    * Pre-conditions :  int index 
    * Post-conditions: const Object &
    *  
    ********************************************/
    const Object & operator[]( int index ) const
    {
                                                     #ifndef NO_CHECK
        if( index < 0 || index >= size( ) )
            throw ArrayIndexOutOfBoundsException{ };
                                                     #endif
        return objects[ index ];
    }

    /********************************************
    * Function Name  : resize
    * Pre-conditions :  int newSize 
    * Post-conditions: none
    *  
    ********************************************/
    void resize( int newSize )
    {
        if( newSize > theCapacity )
            reserve( newSize * 2 );
        theSize = newSize;
    }

    /********************************************
    * Function Name  : reserve
    * Pre-conditions :  int newCapacity 
    * Post-conditions: none
    *  
    ********************************************/
    void reserve( int newCapacity )
    {
        if( newCapacity < theSize )
            return;

        Object *newArray = new Object[ newCapacity ];
        for( int k = 0; k < theSize; ++k )
            newArray[ k ] = std::move( objects[ k ] );

        theCapacity = newCapacity;
        std::swap( objects, newArray );
        delete [ ] newArray;
    }

      // Stacky stuff
    /********************************************
    * Function Name  : push_back
    * Pre-conditions :  const Object & x 
    * Post-conditions: none
    *  
    ********************************************/
    void push_back( const Object & x )
    {
        if( theSize == theCapacity )
            reserve( 2 * theCapacity + 1 );
        objects[ theSize++ ] = x;
    }
      // Stacky stuff
    /********************************************
    * Function Name  : push_back
    * Pre-conditions :  Object && x 
    * Post-conditions: none
    *  
    ********************************************/
    void push_back( Object && x )
    {
        if( theSize == theCapacity )
            reserve( 2 * theCapacity + 1 );
        objects[ theSize++ ] = std::move( x );
    }

    /********************************************
    * Function Name  : pop_back
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    void pop_back( )
    {
        if( empty( ) )
            throw UnderflowException{ };
        --theSize;
    }

    /********************************************
    * Function Name  : back
    * Pre-conditions :  
    * Post-conditions: const Object &
    *  
    ********************************************/
    const Object & back ( ) const
    {
        if( empty( ) )
            throw UnderflowException{ };
        return objects[ theSize - 1 ];
    }

      // Iterator stuff: not bounds checked
    typedef Object * iterator;
    typedef const Object * const_iterator;

    /********************************************
    * Function Name  : begin
    * Pre-conditions :  
    * Post-conditions: iterator
    *  
    ********************************************/
    iterator begin( )
      { return &objects[ 0 ]; }

    /********************************************
    * Function Name  : begin
    * Pre-conditions :  
    * Post-conditions: const_iterator
    *  
    ********************************************/
    const_iterator begin( ) const
      { return &objects[ 0 ]; }

    /********************************************
    * Function Name  : end
    * Pre-conditions :  
    * Post-conditions: iterator
    *  
    ********************************************/
    iterator end( )
      { return &objects[ size( ) ]; }

    /********************************************
    * Function Name  : end
    * Pre-conditions :  
    * Post-conditions: const_iterator
    *  
    ********************************************/
    const_iterator end( ) const
      { return &objects[ size( ) ]; }

    static const int SPARE_CAPACITY = 2;

  private:
    int theSize;
    int theCapacity;
    Object * objects;
};

#endif
